/**
 * @file main.h
 * @brief Function declarations for main.c file.
 * 
 * @section DESCRIPTION
 * 
 * This file contains: function prototypes of functions used only in main.c file;
 * libraries, macros and global variables used everywhere.
 * 
 * @author Žygimantas Kungelis
 * @date 2020-11-17
 * @version 0.3
 * @bug None so far.
*/

#ifndef MAIN
#define MAIN

// Includes
#include "stm32l4xx_hal.h"
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>


// LED pin and port
#define LED_PIN             GPIO_PIN_5
#define LED_PORT            GPIOA

#define BUFFER_SIZE         50 

// Global variables
// GPIO initialisation structs
GPIO_InitTypeDef    GPIO_LED_InitStruct;
GPIO_InitTypeDef    GPIO_UART1_InitStruct;
GPIO_InitTypeDef    GPIO_UART2_InitStruct;
// UART initialisation structs
UART_HandleTypeDef  huart1;
UART_HandleTypeDef  huart2;

/**
 * 
 * @brief Prepares hardware for further coding.
 * 
 * Long decription starts here:
 * Enables clocks, then initiliases GPIO of LED, UART1, UART2
 * and initialises UART1 and UART2 information channels.
 * 
 * @return Void.
*/ 
void GPIO_Init(void);

/**
 * @brief Finds solutions of the equation.
 * 
 * Long decription starts here:
 * Finds solution for the equation in equation array (tx_buffer). 
 * 
 * Assumptions: 
 * 1. equationArray contains a valid equation;  
 * 2. equationSize > 0.
 * 
 * @param tx_buffer an array for saving answers/solutions of the equation;  
 * @param equation an array, containing a quadratic equation (example: "18*x+18*(x^2)-1980=0\\r\\n");
 * @param equationSize the size of equation array. 
 * @return Void. 
*/
void FindSolution(char *tx_buffer, const char *equation, const uint16_t equationSize);

#endif