/**
 * @file functions.h
 * @brief Function declarations for functions.c file.
 * 
 * @section DESCRIPTION
 * 
 * This contains the function prototypes of functions only used in functions.c file.
 * Function prototypes, which are used in both main.c and functions.c, are defined in main.h.
 * 
 * @author Žygimantas Kungelis
 * @date 2020-11-17
 * @version 0.3
 * @bug none so far.
*/



/**
 * @brief Other libraries, variables and function definitions.
 * 
 * More detailed description: 
 * main.h contains libraries (string.h, math.h and others), global variables 
 * and function declarations of functions used only in main.c
 */ 
#include <main.h>

// Variables used only in functions.c

/**
 * @brief Enums used only in gpio_table for clearer code.
 */
enum GPIO_NAMES {
    eGPIO_UART1_RX = 0,
    eGPIO_UART1_TX,
    eGPIO_UART2_RX,
    eGPIO_UART2_TX,
    eGPIO_LD2,
    GPIO_MAX
};

/**
 * @brief A struct for GPIO initialisation.
 * 
 * More detailed explanation:
 * A struct containing all needed information for initialisation of LD2, UART1 and UART2.
 * 
 * @param pin describes the pin number of a specific GPIO;
 * @param mode describes the mode in which the specific GPIO functions;
 * @param alternate describes which alternate function this specific GPIO is using;
 * @param pull describes whether the default state is pull up, down or none;
 * @param speed describes the speed for the selected GPIO pin;
 * @param port describes which port this pin belongs.
 */ 
struct GPIO {
    uint16_t pin;
    uint32_t mode;
    uint8_t alternate;
    uint32_t pull;
    uint32_t speed;
    GPIO_TypeDef *port;
} gpio_table[] = {
    [eGPIO_UART1_RX] = {GPIO_PIN_9, GPIO_MODE_AF_PP, GPIO_AF7_USART1, GPIO_NOPULL, GPIO_SPEED_FREQ_VERY_HIGH, GPIOA},
    [eGPIO_UART1_TX] = {GPIO_PIN_10, GPIO_MODE_AF_OD, GPIO_AF7_USART1, GPIO_NOPULL, GPIO_SPEED_FREQ_VERY_HIGH, GPIOA},
    [eGPIO_UART2_RX] = {GPIO_PIN_2, GPIO_MODE_AF_PP, GPIO_AF7_USART2, GPIO_NOPULL, GPIO_SPEED_FREQ_VERY_HIGH, GPIOA},
    [eGPIO_UART2_TX] = {GPIO_PIN_3, GPIO_MODE_AF_OD, GPIO_AF7_USART2, GPIO_NOPULL, GPIO_SPEED_FREQ_VERY_HIGH, GPIOA},
    [eGPIO_LD2] = {GPIO_PIN_5, GPIO_MODE_OUTPUT_PP, 0, GPIO_PULLUP, GPIO_SPEED_FREQ_HIGH, GPIOA}    
};



/**
 * 
 * @brief Prepares hardware for further coding.
 * 
 * More detailed description:
 * Enables clocks, then initiliases GPIO of LED, UART1, UART2
 * and initialises UART1 and UART2 information channels.
 * 
 * @return Void.
*/ 
void GPIO_Init(void);

/**
 * @brief Enables GPIO and USART clocks.
 * 
 * More detailed explanation:
 * Enables GPIOA, USART1 and USART2 clocks.
 * 
 * @return Void.
*/ 
void Enable_Clocks(void);

/**
 * @brief A blueprint for initialisation of any GPIO element in gpio_table.
 *  
 * @param GPIO_Init_struct gpio init struct;
 * @param specific a specific gpio_table's element.
 * @return Void.
*/ 
void Specific_GPIO_Init(GPIO_InitTypeDef *GPIO_Init_struct, const struct GPIO *specific);

/**
 * @brief Initialisation of UART1 channel.
 * 
 * More detailed explanation:
 * Initialises UART1 baudrate, wordlength, stopbits, parity, 
 * HwFloCtrl, Mode, Oversampling and OneBitSampling 
 * using data in uart_table[eUART_UART1]
 * 
 * @return Void.
*/ 
void UART_UART1_Init(void);

/**
 * @brief Initialisation of UART2 channel.
 * 
 * More detailed explanation:
 * Initialises UART2 baudrate, wordlength, stopbits, parity, 
 * HwFloCtrl, Mode, Oversampling and OneBitSampling 
 * using data in uart_table[eUART_UART2]
 * 
 * @return Void.
*/ 
void UART_UART2_Init(void);

/**
 * @brief Calculates the index of the string we are searching in a given buffer. 
 * 
 * More detailed description:
 * Uses string.h library's function strstr to find the first occurence of 
 * stringSearchingFor in buffer. Then returns -1, if stringSearchingFor is not found,
 * else returns the index of buffer, where the stringSearchingFor begins.
 * 
 * @param buffer a string that may contain the string we are searching for; 
 * @param stringSearchingFor the string we are searching for.
 * @return index (-1 if the string is not found, otherwise positive).
 */ 
int IndexOfStringInAString(const char* buffer, const char* stringSearchingFor);

/**
 * @brief Checks if the flag is true. 
 * 
 * More detailed description:
 * This function is checks if IsTheNumberPositive flag is true, then it returns 1
 * else -1. It is used in ReadCoeficients to simplify the saving of coeficient.
 * 
 * @param isTheNumberPositive a flag for checking if '-' or '+' symbol was before the number in the equation.
 * @return 1 if the flag is true, else -1.
 */ 
int Positivity(const bool isTheNumberPositive);

/**
 * @brief Checks if the flag is true. 
 * 
 * More detailed description:
 * This function is checks if IsTheNumberAFraction flag is true, then it returns -1
 * else 1. It is used in ReadCoeficients to simplify the saving of coeficient.
 * 
 * @param isTheNumberAFraction a flag for checking if '/' symbol was before the number in the equation.
 * @return 1 if the flag is true, else -1.
 */ 
int IsItAFraction(const bool isTheNumberAFraction);

/**
 * @brief Finds all coeficients in a second order polinomial equation. 
 * 
 * More detailed explanation:
 * This function parses the equation and adds coeficients to coefArray.
 * 
 * Assumptions:
 * 1. The equation in equation array is valid (in correct order and has "=0\\r\\n" at the end);
 * 2. coefArray has 3 elements, that are equal to 0.0.
 * 
 * @param equation an array, containing a second order polynomial.
 * @param equationSize the size of equation.
 * @param coefArray array, containing 3 coeficients of the equation (a, b, c).
 * @return Void.
 */ 
void ReadCoeficients(const char *equation, const uint16_t equationSize, float *coefArray);

/**
 * @brief Converts float values to string and saves it in ccoef buffer. 
 * 
 * More detailed explanation:
 * POSITIVE float values are divided into integer and fraction parts. Then using sprintf
 * they are converted to char array, which is concatenated to ccoef. 
 * 
 * Assumptions:
 * 1. floatArraySize > 0.
 * 
 * @param ccoef an array of chars (a string), in which the result of the function will be saved;
 * @param fcoef an array of floats, where the coeficients or solutions of the equation are;
 * @param floatArraySize the size of fcoef array (must be inputed positive).
 * @return Void.
 */ 
void CreateStringFromFloats(char *ccoef, const float *fcoef, const int floatArraySize);

/**
 * @brief Solves second order polinomial equation by using its' coeficients and puts answers in an array. 
 * 
 * More detailed explanation:
 * 
 * Assumptions: 
 * - coefArray contains 3 elements.
 * 
 * @param tx_buffer is an array for transmiting answers/solutions to the equation;  
 * @param equation is an array for equation (example: "18*x+18*(x^2)-1980=0\r\n"); 
 * @param equationSize is the size of equation array. 
 * @return nothing. 
*/
int SolveEquation(const float *coefArray, float *answers);

/**
 * @brief Finds solutions of the equation.
 * 
 * Long decription starts here:
 * Finds solution for the equation in equation array (tx_buffer). 
 * 
 * Assumptions: 
 * 1. equationArray contains a valid equation;  
 * 2. equationSize > 0.
 * 
 * @param tx_buffer an array for saving answers/solutions of the equation;  
 * @param equation an array, containing a quadratic equation (example: "18*x+18*(x^2)-1980=0\\r\\n");
 * @param equationSize the size of equation array. 
 * @return Void. 
*/
void FindSolution(char *tx_buffer, const char *equation, const uint16_t equationSize);