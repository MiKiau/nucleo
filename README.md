# Nucleo L476RG UART 0.3v

## Transmitting a received message to PC

A basic C program for Nucleo STM32L476RG board. It's main purpose is to gain a better understanding of HAL library.

## Description

- This version uses PA9 and PA10 to receive a message (second order polinomial equation) from nano arduino.
- The equation is parsed and solved using basic mathematical formulas.
- Then the positive answers are transmitted using PA2 and PA3 to PC through ST-Link UART. 

## Usage tutorial

Here is a tutorial of how to use this program on Linux:

- Download using: git clone https://gitlab.com/MiKiau/nucleo.git
- Then open the file using VS Code (or other compatable to nucleo STM32L476RG program) with PlatformIO extension using comand: code nucleo &
- Plug in nucleo STM32L476RG to PC and upload the program;
- Connect nucleo's D8 (USART1_TX) to equation sending device's RX slot. Then connect nucleo's D2 (USART1_RX) to equation sending device's TX slot. And finaly connect both nucleo's and the other device's GRD to GRD;
- Connect to nucleo using a connection program (like moserial for Linux);
- Start the equation sending device;
- Enjoy the solutions of the equation sent to your computer.
