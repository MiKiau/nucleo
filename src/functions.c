/**
 * @file functions.c
 * @brief All function bodies of functions declared in functions.h.
 * 
 * @section DESCRIPTION
 * 
 * This file contains the bodies of functions declared in functions.h (and main.h).
 * 
 * @author Žygimantas Kungelis
 * @date 2020-11-17
 * @version 0.3
 * @bug None so far.
*/

// FUNCTIONS.C contains all functions

// Includes
#include "functions.h"

void GPIO_Init(void) {
    // Enable clocks for GPIOs and UARTs
    Enable_Clocks();

    // Initialise all GPIOs
    Specific_GPIO_Init(&GPIO_LED_InitStruct, &gpio_table[eGPIO_LD2]);
    Specific_GPIO_Init(&GPIO_UART1_InitStruct, &gpio_table[eGPIO_UART1_RX]);
    Specific_GPIO_Init(&GPIO_UART1_InitStruct, &gpio_table[eGPIO_UART1_TX]);
    Specific_GPIO_Init(&GPIO_UART2_InitStruct, &gpio_table[eGPIO_UART2_RX]);
    Specific_GPIO_Init(&GPIO_UART2_InitStruct, &gpio_table[eGPIO_UART2_TX]);

    // Initialise all UART connections
    UART_UART1_Init();
    UART_UART2_Init();
}

void Enable_Clocks(void) {
    // GPIO port A clock
    __HAL_RCC_GPIOA_CLK_ENABLE();

    // USART1 and USART2 clocks
    __USART1_CLK_ENABLE();
    __USART2_CLK_ENABLE();
}

void Specific_GPIO_Init(GPIO_InitTypeDef *GPIO_Init_struct, const struct GPIO *specific) {
    GPIO_Init_struct->Pin = specific->pin;
    GPIO_Init_struct->Mode = specific->mode;
    GPIO_Init_struct->Alternate = specific->alternate;
    GPIO_Init_struct->Pull = specific->pull;
    GPIO_Init_struct->Speed = specific->speed;
    HAL_GPIO_Init(specific->port, GPIO_Init_struct);
}

void UART_UART1_Init(void) {
    // Configurating the UART1 connection parameters
    huart1.Instance = USART1;
    huart1.Init.BaudRate = 9600;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    // Specifies whether the Receive or Transmit mode is enabled or disabled.
    huart1.Init.Mode = UART_MODE_TX_RX;
    // Specifies whether the Over sampling 8 is enabled or disabled, to achieve higher speed (up to f_PCLK/8).
    huart1.Init.OverSampling = UART_OVERSAMPLING_8;
    // Specifies whether a single sample or three samples' majority vote is selected. Selecting the single sample method increases the receiver tolerance to clock deviations.
    huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;

    HAL_UART_Init(&huart1);
    
    // Configurate USART1 interrupt priority
    HAL_NVIC_SetPriority(USART1_IRQn, 5, 0);
    // Enabling NVIC
    HAL_NVIC_EnableIRQ(USART1_IRQn);
}

void UART_UART2_Init(void) {
    // Configurating the UART2 connection parameters
    huart2.Instance = USART2;
    huart2.Init.BaudRate = 9600;
    huart2.Init.WordLength = UART_WORDLENGTH_8B;
    huart2.Init.StopBits = UART_STOPBITS_1;
    huart2.Init.Parity = UART_PARITY_NONE;
    huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart2.Init.Mode = UART_MODE_TX_RX;
    huart2.Init.OverSampling = UART_OVERSAMPLING_8;
    huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;

    HAL_UART_Init(&huart2);

    // Configurate USART2 interrupt priority
    HAL_NVIC_SetPriority(USART2_IRQn, 5, 0);
    // Enabling NVIC
    HAL_NVIC_EnableIRQ(USART2_IRQn);
}

int IndexOfStringInAString(const char* buffer, const char* stringSearchingFor) {
    char* pointerToFirstOccurence = strstr(buffer, stringSearchingFor);
    if (pointerToFirstOccurence != NULL) {
        // Equation length - the length from pointer to the end = the length from beginning to pointer.
        int index = abs((int)(strlen(buffer) - strlen(pointerToFirstOccurence)));
        return index;
    } 
    else return -1;
}

int Positivity(const bool isTheNumberPositive) {
    return ((isTheNumberPositive) ? 1 : -1);
}

int IsItAFraction(const bool isTheNumberAFraction) {
    return ((isTheNumberAFraction) ? -1 : 1);
}

void ReadCoeficients(const char *equation, const uint16_t equationSize, float *coefArray) {
    // Temp variables
    uint16_t bufferSize = 10;
    char numberBuffer[bufferSize];
    bool isTheNumberPositive = true;
    bool isTheNumberAFraction = false;
    uint16_t index = 0;
    uint16_t putNumberIn = 3;

    // Clean the buffer
    for (uint16_t i = 0; i < bufferSize; i++)
        numberBuffer[i] = '\0';

    // Go through the whole equation
    for (uint16_t current = 0; current < equationSize; current++) {
        char csymbol = equation[current];
        // Is the current symbol a digit?
        if (isdigit(csymbol) != 0) {
            // If the digit is 2 in expression "(x^2)", then ignore it.
            if (current > 0 && (equation[current + 1] == ')' && equation[current - 1] == '^'))
                continue;
            // Save it
            numberBuffer[index++] = csymbol;
        }
        // The current symbol is not a digit
        else {
            // If the current symbol is '/', then this is a fraction
            if (csymbol == '/') {
                isTheNumberAFraction = true;

                if (equation[current - 1] == 'x') {
                    // This is a coeficient of x
                    putNumberIn = 1;
                }
                else if (equation[current - 1] == ')') {
                    // This is a coeficient of (x^2)
                    putNumberIn = 0;
                }
            }
            // If it is any other symbol
            else {
                // If the buffer is filled and if the symbol is one of "+-*=", 
                // then decide where to put the number
                if (index != 0 && strchr("+-*=", csymbol) != NULL) {
                    // General formula: a*(x^2)+b*x+c=0\r\n

                    // if this is true, then this is c
                    if (csymbol == '=')
                        putNumberIn = 2;
                    // if this is true, then this is b
                    else if (csymbol == '*' && equation[current + 1] == 'x')
                        putNumberIn = 1;
                    // if this is true, then this is a
                    else if (csymbol == '*' && equation[current + 1] == '(')
                        putNumberIn = 0;
                
                    // If the number is finished, then add it to it's belonging place
                    if (putNumberIn != 3) {
                        coefArray[putNumberIn] += Positivity(isTheNumberPositive) * pow(atoi(numberBuffer), IsItAFraction(isTheNumberAFraction));

                        // If the current symbol is '=', then the equation is eveluated
                        if (csymbol == '=')
                            break;

                        // Reset temp variables to default values
                        for (int i = 0; i < bufferSize; i++)
                            numberBuffer[i] = '\0';
                        isTheNumberPositive = true;
                        isTheNumberAFraction = false;
                        index = 0;
                        putNumberIn = 3;
                    } // Addition and cleaning up ends here.
                }

                // Check if this symbol decides the next number's positivity
                if (csymbol == '+')
                    isTheNumberPositive = true;
                else if (csymbol == '-')
                    isTheNumberPositive = false;
            } // Deciding what to with this symbol ends here.
        } // Deciding whether the symbol is a digit ends here.
    } // for cycle ends here.
}

void CreateStringFromFloats(char *ccoef, const float *fcoef, const int floatArraySize) {
    int count = 1;
    for (int i = 0; i < floatArraySize; i++) {
        // Only positive numbers
        if (fcoef[i] < 0)
            continue;

        char expression[50];
        float valueToConvert = fcoef[i];

        // Integer part of the original value
        int intValue = valueToConvert;
        // Fractional part of the original value
        float fracValue = valueToConvert - intValue;
        int intFracValue = trunc(fracValue * 100);

        sprintf(expression, "(%d) %d.%02d; ", count++, intValue, intFracValue);
        strcat(ccoef, expression);
    }
    strcat(ccoef, "\r\n");
}

int SolveEquation(const float *coefArray, float *answers) {
    // Check if the coeficients are valid (a and b can't be both equal to 0.0)
    if ((coefArray[0] == 0.0 && coefArray[1] == 0.0 && coefArray[2] == 0.0) || (coefArray[0] == 0.0 && coefArray[1] == 0.0)) {
        // Wrong coeficients
        return -1;
    }

    // The coeficients are valid, so calculate the answers
    int answersNumber = 0;
    if ((coefArray[1] == 0.0 && coefArray[2] == 0.0) || (coefArray[0] == 0.0 && coefArray[2] == 0.0)) {
        // ax^2 = 0 || bx = 0 => 1 answer => 0.0
        answers[answersNumber++] = 0.0;
    }
    else if (coefArray[0] == 0.0) {
        // bx + c = 0 => 1 answer => (-1 * c) / b
        answers[answersNumber++] = (-1 * coefArray[2]) / coefArray[1];
    }
    else if (coefArray[1] == 0.0) {
        // ax^2 + c = 0 => 0 or 2 answers
        float divisionResult = coefArray[2] / coefArray[0];
        if (divisionResult > 0) {
            // x^2 + d = 0 => no real answers
        }
        else {
            // x^2 - d = 0 => 2 answers => sqrt(d) and -1 * sqrt(d)
            answers[answersNumber++] = sqrt(-1 * divisionResult);
            answers[answersNumber] = -1 * answers[answersNumber - 1];
            answersNumber++;
        }
    }
    else if (coefArray[2] == 0.0) {
        // ax^2 + bx = 0 <=> x*(ax + b) = 0
        // 2 answers => 0 and (-1 * b) / a
        answers[answersNumber++] = 0.0;
        answers[answersNumber++] = (-1 * coefArray[1]) / coefArray[0];
    }
    else {
        // ax^2 + bx + c = 0 => 0, 1 or 2 answers
        float delta = pow(coefArray[1], 2) - 4 * coefArray[0] * coefArray[2];
        if (delta < 0) {
            // No real answers
        }
        else if (delta == 0) {
            // 1 answer => (-1 * b) / (2 * a)
            answers[answersNumber++] = (-1 * coefArray[1]) / (2 * coefArray[0]);
        }
        else {
            // 2 answers => (-1 * b) / (2 * a) +- sqrt(delta) / (2 * a)
            answers[answersNumber++] = (-1 * coefArray[1] + sqrt(delta)) / (2 * coefArray[0]);
            answers[answersNumber++] = (-1 * coefArray[1] - sqrt(delta)) / (2 * coefArray[0]);
        }
    }
    return answersNumber;
}

void FindSolution(char *tx_buffer, const char *equation, const uint16_t equationSize) {
    // Finding coeficients
    float coefArray[3] = {0.0, 0.0, 0.0};
    ReadCoeficients(equation, equationSize, coefArray);
    
    // Create a message
    char coefMessage[100] = "";
    
    // find solutions
    float answers[2] = {0.0, 0.0};
    int answersNumber = SolveEquation(coefArray, answers);

    // Simple check for errors
    if (answersNumber < 0) {
        // Wrong input!!
        sprintf(coefMessage, "WRONG COEFICIENTS!\r\n");
    } 
    else {
        CreateStringFromFloats(coefMessage, answers, answersNumber);
    }

    // Clean transfer buffer
    for (uint16_t current = 0; current < BUFFER_SIZE; current++)
        tx_buffer[current] = '\0';

    // Transfering coeficients to transmitting buffer
    strcpy(tx_buffer, coefMessage);
}