/**
 * @file main.c
 * @brief A program for improving understanding of how to program in embedded systems.
 * 
 * @section DESCRIPTION
 * 
 * A program written for Nucleo STM32L476RG board to do the following things:
 * 1. Receive a message through USART1, containing a second order polynomial equation;
 * 2. Parse and solve the equation.
 * 3. Send the positive answers through USART2 to PC or other device.
 * 
 * @author Žygimantas Kungelis
 * @date 2020-11-17
 * @version 0.3
 * @bug All known bugs are eradicated.
*/


// Includes
#include "main.h"

// Private global variables
// Buffers
uint8_t rx_buffer[BUFFER_SIZE];
uint8_t tx_buffer[BUFFER_SIZE];
// Received char
uint8_t symbol = '\0';
// Equation size
uint16_t equationSize = 0;

// Flags for program control
volatile bool isTheMessageReceived = false;
volatile bool isTheMessageCompleted = false;
volatile bool isTheMessageSent = false;


int main(void) {
    // Initialise HAL
    HAL_Init();

    // Initialise all GPIOs
    GPIO_Init();

    // Start receiving data through UART1
    HAL_UART_Receive_IT(&huart1, &symbol, 1);

    while (1) {
        if (isTheMessageReceived) {
            isTheMessageReceived = false;

            // Copy the received symbol and push the length count
            rx_buffer[equationSize++] = symbol;

            if (symbol == '\n') {
                // The message is complete
                isTheMessageCompleted = true;
            }
            else {
                // The message was not completed, then continue receiving symbols
                HAL_UART_Receive_IT(&huart1, &symbol, 1);
            }
        }

        if (isTheMessageCompleted) {
            isTheMessageCompleted = false;

            // Find solution(s)
            FindSolution((char*)tx_buffer, (char*)rx_buffer, equationSize);

            // Reset the length counter
            equationSize = 0;

            // Transmit answers
            HAL_UART_Transmit_IT(&huart2, tx_buffer, (uint16_t)strlen((char*)tx_buffer));
        }
        
        if (isTheMessageSent) {
            isTheMessageSent = false;

            // Blink LED as indication of successful transfer
            HAL_GPIO_TogglePin(LED_PORT, LED_PIN);
            
            // Restart receiving data
            HAL_UART_Receive_IT(&huart1, &symbol, 1);
        }
    }
}

// This function is called after a successful UART_Receive
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
    if (huart == &huart1) {
        isTheMessageReceived = true;
    }
}

// This function is called after a successful UART_Transmit
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
    if (huart == &huart2) {
        isTheMessageSent = true;
    }
}

void SysTick_Handler(void) {
    HAL_IncTick();
}

void USART1_IRQHandler(void) {
    HAL_UART_IRQHandler(&huart1);
}

void USART2_IRQHandler(void) {
    HAL_UART_IRQHandler(&huart2);
}